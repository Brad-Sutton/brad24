// Bradley Sutton
#include <stdio.h>

int main(int argc, char *argv[]) {

	setvbuf(stdout, NULL, _IONBF, 0);
	setvbuf(stderr, NULL, _IONBF, 0);

	size_t i = 0;
	scanf("%zu", &i);
	printf("Value %zu", i);
	return (0);
}


